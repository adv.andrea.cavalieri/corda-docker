FROM acavalieri/corda-zulu-java1.8-4.7-snapshot:latest
LABEL maintainer "Cavalieri <cavalieri@ccprogetti>"

USER root
RUN whoami
#RUN ls /opt/corda
RUN cat /etc/passwd

#RUN chgrp -R 0 /opt/corda 

ADD cordapps/*.jar /opt/corda/cordapps/
##ADD network-bootstrapper/notary/corda.jar /opt/corda/corda.jar
ADD additional-node-infos/* /opt/corda/additional-node-infos/
ADD node.conf /etc/corda/node.conf
ADD certificates/* /opt/corda/certificates/
ADD custom-log4j2.xml /opt/corda/custom-log4j2.xml
ADD shared/drivers/*.jar /opt/corda/drivers/
ADD network-parameters /opt/corda/network-parameters

RUN chmod -R a+rwX /opt/corda
RUN ls -lrt /opt/corda/bin

ENV CORDA_ARGS="--log-to-console --no-local-shell"
ENV JVM_ARGS="${JVM_ARGS} -Dlog4j.configurationFile=/opt/corda/custom-log4j2.xml"
USER 1001
CMD ["run-corda"]
